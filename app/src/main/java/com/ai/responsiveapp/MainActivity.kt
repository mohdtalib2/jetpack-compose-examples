package com.ai.responsiveapp

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.Typography
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.ai.responsiveapp.ui.theme.ResponsiveAppTheme
import com.ai.responsiveapp.ui.theme.Typography
import com.ai.responsiveapp.ui.theme.lightBackground

class MainActivity : ComponentActivity() {

    private val viewModel: MainActivityViewModel by viewModels()
    var videoList = mutableListOf<Videos>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ResponsiveAppTheme {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(lightBackground)
                ) {
                    Column(modifier = Modifier.fillMaxSize()) {
                        VideoPlaylist()
                    }
                }
            }
        }
    }


    @Composable
    fun VideoPlaylist() {

        val videoList1 by viewModel.getAllVideos().observeAsState(listOf())
        if (videoList1.isNotEmpty()) {
            videoList = videoList1 as MutableList<Videos>
        }


        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .padding(0.dp, 10.dp, 0.dp, 5.dp)

        ) {
            itemsIndexed(videoList) { index, video ->
                PlaylistRow(video, index)
            }

        }
    }

    @Composable
    fun PlaylistRow(video: Videos, index: Int) {

        BoxWithConstraints {
            val boxWithConstraintsScope = this

            if (boxWithConstraintsScope.maxWidth > 480.dp) {
                Row(modifier = Modifier
                    .padding(10.dp, 5.dp, 10.dp, 5.dp),
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically) {
                    Image(
                        modifier = Modifier
                            .width(150.dp)
                            .height(130.dp),
                        contentScale = ContentScale.Crop,
                        painter = rememberImagePainter(data = video.imageUrl,
                            builder = {
                                crossfade(false)
                                placeholder(R.drawable.placeholder)
                            }),
                        contentDescription = null,
                    )
                    Text(
                        modifier = Modifier
                            .wrapContentWidth()
                            .padding(10.dp, 0.dp, 10.dp, 0.dp),
                        text = video.title,
                        color = Color.Black,
                        style = Typography.body1
                    )

                }
            }
            else {
                Column(modifier = Modifier
                    .padding(0.dp, 10.dp, 0.dp, 5.dp),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally) {

                    Image(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(180.dp),
                        contentScale = ContentScale.Crop,
                        painter = rememberImagePainter(data = video.imageUrl,
                            builder = {
                                crossfade(false)
                                placeholder(R.drawable.placeholder)
                            }),
                        contentDescription = null,
                    )

                    Text(modifier = Modifier
                        .fillMaxWidth()
                        .padding(10.dp, 5.dp, 10.dp, 0.dp),
                        text = video.title,
                        color = Color.Black,
                        style = Typography.body1
                    )


                }
            }
        }

    }


    @Preview(showBackground = true)
    @Composable
    fun DefaultPreview() {
        ResponsiveAppTheme {
            VideoPlaylist()
        }
    }
}