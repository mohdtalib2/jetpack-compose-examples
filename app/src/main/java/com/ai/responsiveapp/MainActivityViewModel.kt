package com.ai.responsiveapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


class MainActivityViewModel: ViewModel() {

    private var liveData: LiveData<List<Videos>>
    private var videoListRepository: VideoListRepository

    init {
        liveData = MutableLiveData()
        videoListRepository = VideoListRepository()
    }

    fun getAllVideos(): LiveData<List<Videos>> {
        liveData = videoListRepository.makeApiCall()
        return liveData
    }

}
