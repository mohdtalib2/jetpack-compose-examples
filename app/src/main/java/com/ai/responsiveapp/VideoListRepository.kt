package com.ai.responsiveapp

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class VideoListRepository {

    fun makeApiCall(): LiveData<List<Videos>> {
        val videosList = MutableLiveData<List<Videos>>()

        val videoList = ArrayList<Videos>()
        videoList.add(Videos("Big Buck Bunny", "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/BigBuckBunny.jpg"))
        videoList.add(Videos("Elephant Dream", "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/ElephantsDream.jpg"))
        videoList.add(Videos("For Bigger Blazes", "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/ForBiggerBlazes.jpg"))
        videoList.add(Videos("For Bigger Escape", "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/ForBiggerEscapes.jpg"))

        videosList.value = videoList

        return videosList
    }


}